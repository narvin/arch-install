#!/bin/bash

set -euo pipefail

DIR="$(dirname "$0")"
source "${DIR}"/../../lib/util

# shellcheck disable=SC2086
pacman -Sy ${1:-} --needed \
  xorg-server \
  xorg-xinit \
  xorg-xrandr \
  xorg-xsetroot \
  libxft \
  adobe-source-code-pro-fonts \
  ttf-font-awesome \
  picom \
  feh \
  qemu \
  virt-manager \
  qemu-arch-extra \
  ovmf \
  ebtables \
  dnsmasq \
  openbsd-netcat \
  bridge-utils \
  zsh \
  vim \
  git \
  openssh \
  firefox >&3

set_bash_rcpath
set_bash_term
set_zsh_dotdir
set_zsh_term
set_root_bashrc
rm_skel .bashrc
link_vi_to_vim
harden_sshd_config

link_libvirt_dirs_to_data_dirs
add_iommu_kernel_params
update_libvirtd_conf_for_passthrough
install_qemu_passthrough_hooks

systemctl enable libvirtd sshd

