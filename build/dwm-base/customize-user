#!/bin/bash

set -euo pipefail

declare -r user="${1:-}"
declare -r user_passwd="${2:-}"
declare -r dot_repo="${3:-}"

declare asset_dir; asset_dir="$(pwd)/$(dirname "$0")"; readonly asset_dir

# Set shell
printf '%b\n' "${user_passwd}" | chsh -s "$(command -v zsh)" "${user}"

# Clone dotfiles repo
if [[ -n "${dot_repo}" ]]; then
  git clone "${dot_repo}" /home/"${user}"/.config
fi

# Clone source code repos, compile and install applications
mkdir /home/"${user}"/src
# dwm
git clone https://git.suckless.org/dwm /home/"${user}"/src/dwm
cd /home/"${user}"/src/dwm
git checkout 6.2
[[ -f "${asset_dir}/config.h" ]] && cp "${asset_dir}/config.h" ./
[[ -r "${asset_dir}/dwm.diff" ]] && patch < "${asset_dir}"/dwm.diff
printf '%b\n' "${user_passwd}" | sudo -S make clean install
# dmenu
git clone https://git.suckless.org/dmenu /home/"${user}"/src/dmenu
cd /home/"${user}"/src/dmenu
sed -e 's/^\(XINERAMA\)/# \1/' -i config.mk
printf '%b\n' "${user_passwd}" | sudo -S make clean install

# Configure .xinitrc to start dwm and other applications on startx
{
  head -n -5 /etc/X11/xinit/xinitrc
  printf 'picom &\n'
  printf 'nitrogen --restore &\n'
  printf 'export SHLVLOFFSET=2\n'
  printf 'exec dwm\n'
} > /home/"${user}"/.xinitrc
printf 'Created /home/%s/.xinitrc\n' "${user}"

is_done=1

